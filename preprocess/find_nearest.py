import os

import numpy as np
import torch

import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader


class VideoEmbeddingDataset(Dataset):
    def __init__(self, root):
        self.root = root
        self.vid_feat_file = list(os.listdir(self.root))

    def __len__(self):
        return len(self.vid_feat_file)

    def __getitem__(self, index):
        video_emb = np.load(os.path.join(self.root, self.vid_feat_file[index])).mean(0)
        video_id = int(self.vid_feat_file[index].split('.')[0])
        return video_emb, video_id


if __name__ == '__main__':
    dataset = VideoEmbeddingDataset(root="/home/wangbo18/wangjinpeng/vid2seq/features/MSVD/visual/clip-vit-L-14")
    dataloader = DataLoader(dataset, batch_size=16, shuffle=False)