# Import moviepy
import os
import moviepy.editor
from tqdm import tqdm

if __name__ == '__main__':
    for subset in ['TrainValVideo']:
        root = f"/home/wangbo18/wangjinpeng/Datasets/MSRVTT/{subset}/"
        save_root = f"/home/wangbo18/wangjinpeng/vid2seq/features/MSRVTT/audio/raw/{subset}/"
        all_count = 0
        valid_count = 0
        for fname in tqdm(os.listdir(root)):
            fpath = os.path.join(root, fname)
            video = moviepy.editor.VideoFileClip(fpath)
            all_count += 1
            if video.audio is not None:
                # fps is what whisper-large-v2 (ASR model) requires
                video.audio.write_audiofile(os.path.join(save_root, fname.split('.')[0] + ".wav"), fps=16000)
                valid_count += 1
        print(f"Valid rate of {subset}:{valid_count / all_count * 100:.2f}\%")
    
