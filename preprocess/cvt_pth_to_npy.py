import os, sys
import numpy as np
import torch

if __name__ == '__main__':
    dataset = sys.argv[1]
    data = torch.load(f"../features/{dataset}/visual/clip-vit-L-14/clipvitl14.pth")
    for key, val in data.items():
        np.save(f"../features/{dataset}/visual/clip-vit-L-14/{key}.npy", val)
