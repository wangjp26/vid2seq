import os
import argparse
from itertools import chain
from tqdm import tqdm
import json as js
import numpy as np
import pandas as pd
from PIL import Image
import torch
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import clip


class ImageDataset(Dataset):
    def __init__(self, root):
        self.root = root
        self.images = list(os.listdir(self.root))
        self.img_transforms = transforms.Compose([
            transforms.Resize(256), # the video is center-cropped
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            # transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index):
        image_path = os.path.join(self.root, self.images[index])
        filename = os.path.splitext(self.images[index])[0]
        image = Image.open(image_path).convert('RGB')
        image = self.img_transforms(image)
        return image, filename


def parser_args():
    parser = argparse.ArgumentParser(description="Easy video feature extractor")

    parser.add_argument(
        "--img_root",
        type=str,
        help="root of image dataset",
        default='/home/wangbo18/wangjinpeng/Datasets/COCO2017/images/',
    )
    parser.add_argument(
        "--coco_anno_path",
        type=str,
        help="root of image dataset",
        default='/home/wangbo18/wangjinpeng/Datasets/COCO2017/dataset_coco.json',
    )
    parser.add_argument(
        "--model_path",
        type=str,
        help="path of CLIP-ViT",
        default='/home/wangbo18/wangjinpeng/Datasets/PretrainedModels/',
    )
    parser.add_argument(
        "--save_root",
        type=str,
        help="the directory to save features",
        default='/home/wangbo18/wangjinpeng/vid2seq/features/MSCOCO/visual/',
    )
    parser.add_argument(
        "--batch_size", type=int, default=1024, help="batch size for extraction"
    )
    parser.add_argument(
        "--half_precision",
        type=int,
        default=1,
        help="whether to output half precision float or not",
    )
    parser.add_argument(
        "--l2_normalize",
        type=int,
        default=0,
        help="whether to l2 normalize the output feature",
    )

    return parser.parse_args()


def make_coco_kv_pairs(coco_anno_path=None):
    with open(coco_anno_path) as f:
        data = js.load(f)['images']
    new_data = []
    for item in data:
        new_item = {}
        new_item['cocoid'] = item['cocoid']
        # only use the first caption
        new_item['sentence'] = item['sentences'][0]['raw']
        new_data.append(new_item)
    new_data = pd.DataFrame(new_data)
    return new_data, set(new_data.cocoid)


if __name__ == '__main__':
    args = parser_args()

    # dataset & dataloader
    dataset = ImageDataset(root=args.img_root)
    dataloader = DataLoader(dataset, batch_size=args.batch_size, shuffle=False)

    # get coco ids
    coco_kv_df, coco_id_set = make_coco_kv_pairs(args.coco_anno_path)

    # define model
    model, _ = clip.load("ViT-L/14", download_root=args.model_path)
    model.eval()
    model = model.cuda()

    # inference
    all_features = []
    all_filenames = []
    with torch.no_grad():
        for data in tqdm(dataloader):
            images, filenames = data
            features = model.encode_image(images.cuda())
            if args.l2_normalize:
                features = F.normalize(features, dim=1)
            features = features.cpu().numpy()
            if args.half_precision:
                features = features.astype("float16")
            all_features.append(features)
            all_filenames.append(filenames)
        del model
    
    # save features
    os.makedirs(args.save_root, exist_ok=True)
    all_features = np.concatenate(all_features)
    all_filenames = chain(*all_filenames)
    for idx, filename in enumerate(tqdm(all_filenames)):
        file_id = int(filename)
        if file_id in coco_id_set:
            np.save(os.path.join(args.save_root, f'{file_id}.npy'), all_features[idx])
        else:
            print(f"File_id: '{file_id}' not in COCO ID set.")