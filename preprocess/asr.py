from transformers import WhisperProcessor, WhisperForConditionalGeneration
from datasets import load_dataset
import numpy as np
import sys

def prepare_dataset(batch):
    audio = batch["audio"]
    fname = [au["path"] for au in audio]
    input_features = processor(
        [au["array"] for au in audio], 
        sampling_rate=16000, # whisper-large-v2 setup
        return_tensors="pt").input_features
    predicted_ids = model.generate(input_features.to(gpu))
    transcriptions = processor.batch_decode(predicted_ids, skip_special_tokens=True)
    batch["asr"] = transcriptions
    batch["fname"] = fname
    return batch

if __name__ == '__main__':
    split = sys.argv[1]
    gpu = "cuda:" + sys.argv[2]
    
    # load model and processor
    print("Preparing ASR model.")
    processor = WhisperProcessor.from_pretrained("openai/whisper-large-v2")
    model = WhisperForConditionalGeneration.from_pretrained("openai/whisper-large-v2").to(gpu)
    # model.config.forced_decoder_ids = None
    model.config.forced_decoder_ids = processor.get_decoder_prompt_ids(language="english", task="transcribe")

    # load dummy dataset and read audio files
    print("Preparing dataset.")
    dataset = load_dataset("audiofolder", 
                           data_dir="/home/wangbo18/wangjinpeng/vid2seq/features/MSRVTT/audio/raw", 
                           drop_labels=True, split=split)

    # generate token ids
    print("Map dataset.")
    dataset = dataset.map(prepare_dataset, batched=True, batch_size=32, remove_columns=dataset.column_names)
    dataset.to_csv(f"/home/wangbo18/wangjinpeng/vid2seq/features/MSRVTT/audio/transcriptions/{split}.csv")
