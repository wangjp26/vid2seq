import json as js
import pandas as pd
import os, sys, re
import numpy as np
import moviepy.editor
import pickle as pk
from copy import deepcopy

def MSRVTT():
    # headers = ['video_id', 'duration', 'caption', 'start', 'end', 'asr_string', 'asr_start', 'asr_end', 'features']
    for js_fname, asr_fname in [('train_val', 'train'), ('test', 'test')]:
        with open(f'/home/wangbo18/wangjinpeng/Datasets/MSRVTT/{js_fname}_videodatainfo.json') as f:
            jsf = js.load(f)
            # 'category', 'url', 'video_id', 'start time', 'end time', 'split', 'id'
            df_data = pd.DataFrame(jsf["videos"])
            # 'caption', 'video_id', 'sen_id'
            df_captions = pd.DataFrame(jsf["sentences"]).drop(columns='sen_id').groupby('video_id').agg(list).reset_index()
            # 'category', 'url', 'video_id', 'start time', 'end time', 'split', 'id', 'caption'
            df_data = pd.merge(df_data, df_captions, on='video_id')
        df_data["start"] = None
        df_data["duration"] = ((df_data['end time'] - df_data['start time']) * 1000).astype(int)
        df_data["end"] = None
        # 'video_id', 'duration', 'start', 'end', 'id'
        df_data.drop(columns=['category', 'url', 'start time', 'end time', 'split'], inplace=True)
        df_ASR_data = pd.read_csv(f"../features/MSRVTT/audio/transcriptions/{asr_fname}.csv") # 'asr', 'fname'
        df_ASR_data["id"] = df_ASR_data["fname"].map(lambda video_id: int(re.search('\d+', os.path.split(video_id)[1]).group()))
        df_ASR_data["asr_string"] = df_ASR_data["asr"].map(lambda x: [x.strip()])
        # 'asr_string', 'id'
        df_ASR_data.drop(columns=['fname', 'asr'], inplace=True)
        df = pd.merge(df_data, df_ASR_data, how="left", on="id")
        df.drop(columns='id', inplace=True) # 'video_id', 'duration', 'start', 'end', 'asr_string', 'caption'
        # w/o: asr_start, asr_end, features
        df["asr_start"] = None
        df["asr_end"] = None
        def row_func(row):
            row["start"] = [0]
            row['end'] = [row["duration"]]
            if isinstance(row['asr_string'], list):
                row['asr_start'] = deepcopy(row['start'])
                row['asr_end'] = deepcopy(row['end'])
            else:
                row['asr_string'] = []
                row['asr_start'] = []
                row['asr_end'] = []
            return row
        # w/o: features
        df = df.apply(row_func, axis=1)
        df.to_csv("../msrvtt_" + asr_fname + '_data.csv')

def make_msvd_splits():
    with open("/home/wangbo18/wangjinpeng/Datasets/MSVD/all_captions.txt") as f:
        raw_captions = f.readlines()
    with open("/home/wangbo18/wangjinpeng/Datasets/MSVD/all_captions.csv", 'w') as f:
        print("video,caption", file=f)
        for line in raw_captions:
            split_index = re.search('^\S+\d\s', line).span()[1]
            print(line[:split_index-1]+','+line[split_index:], end='', file=f)
    # video, caption
    df_caption = pd.read_csv("/home/wangbo18/wangjinpeng/Datasets/MSVD/all_captions.csv").groupby("video").agg(list)
    with open("/home/wangbo18/wangjinpeng/Datasets/MSVD/train_list.txt") as f:
        train_val_list = list(map(str.strip, f.readlines()))
    with open("/home/wangbo18/wangjinpeng/Datasets/MSVD/val_list.txt") as f:
        train_val_list += list(map(str.strip, f.readlines()))
    with open("/home/wangbo18/wangjinpeng/Datasets/MSVD/test_list.txt") as f:
        test_list = list(map(str.strip, f.readlines()))
    df_train_val_caption = df_caption.loc[train_val_list].reset_index()
    df_test_caption = df_caption.loc[test_list].reset_index()
    with open("/home/wangbo18/wangjinpeng/Datasets/MSVD/structured-symlinks/dict_youtube_mapping.pkl", 'rb') as f:
        name_to_vidid = pk.load(f)
    df_train_val_caption["video_id"] = df_train_val_caption["video"].map(name_to_vidid)
    df_train_val_caption["video_id"] = df_train_val_caption["video_id"].map(lambda video_id: int(re.search('\d+', os.path.split(video_id)[1]).group()))
    df_test_caption["video_id"] = df_test_caption["video"].map(name_to_vidid)
    df_test_caption["video_id"] = df_test_caption["video_id"].map(lambda video_id: int(re.search('\d+', os.path.split(video_id)[1]).group()))
    return df_train_val_caption, df_test_caption # video, video_id, caption

def MSVD():
    df_train_caption, df_test_caption = make_msvd_splits()
    print(df_train_caption)
    df_train_caption["duration"] = None
    df_train_caption["start"] = None
    df_train_caption["end"] = None
    df_train_caption["asr_string"] = None
    df_train_caption["asr_start"] = None
    df_train_caption["asr_end"] = None
    df_test_caption["duration"] = None
    df_test_caption["start"] = None
    df_test_caption["end"] = None
    df_test_caption["asr_string"] = None
    df_test_caption["asr_start"] = None
    df_test_caption["asr_end"] = None

    def row_func(row):
        # MSVD dataset has no audio
        row['asr_string'] = []
        row['asr_start'] = []
        row['asr_end'] = []
        fpath = os.path.join("/home/wangbo18/wangjinpeng/Datasets/MSVD/YouTubeClips", row['video'] + '.avi')
        video = moviepy.editor.VideoFileClip(fpath)
        row['start'] = [0]
        row['duration'] = int((video.end - video.start) * 1000)
        row['end'] = [row['duration']]
        return row
    # w/o: features
    df_train_caption = df_train_caption.apply(row_func, axis=1)
    df_test_caption = df_test_caption.apply(row_func, axis=1)
    print(df_train_caption)
    df_train_caption.drop(columns='video', inplace=True)
    df_test_caption.drop(columns='video', inplace=True)
    df_train_caption.to_csv("../msvd_train_data.csv")
    df_test_caption.to_csv("../msvd_test_data.csv")

if __name__ == '__main__':
    MSRVTT()
    MSVD()